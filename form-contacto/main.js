$(document).ready(function(){

    $('#btnSend').click(function(){ 

        var errores = '';

        if($('#nombre').val() == ''){
            errores += '<p>Debe escribir un nombre.</p>'; 
            $('#nombre').css('border-bottom-color', '#F14B4B');
        }else{
            $('#nombre').css('border-bottom-color', '#D1D1D1');
        }
     
        if($('#email').val() == ''){
            errores += '<p>Debe escribir un email.</p>';
            $('#email').css('border-bottom-color', '#F14B4B');
        }else{
            $('#email').css('border-bottom-color', '#D1D1D1');
        }
        
        if($('#mensaje').val() == ''){
            errores += '<p>Debe escribir un mensaje.</p>';
            $('#mensaje').css('border-bottom-color', '#F14B4B');
        }else{
            $('#mensaje').css('border-bottom-color', '#D1D1D1');
        }
        
        if(errores != ''){
            var mensajeModal = '<div class="modal_wrap"><div class="mensaje_modal"><h3>Errores encontrados</h3>';
            mensajeModal += errores;
            mensajeModal += '<span id="btnClose">Cerrar</span>';
            mensajeModal += '</div></div>';

            $('body').append(mensajeModal);
        }

        $('#btnClose').click(function(){
            $('.modal_wrap').remove();
        });


    });

});